package com.arun.aashu.dialogbox;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {




    AlertDialog.Builder ab;
    int count;

    @Override
    public void onBackPressed() {

        count++;
        if (count == 1) {

            Toast.makeText(this, "press again", Toast.LENGTH_SHORT).show();
        } else if (count == 2) {

            super.onBackPressed();

        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ab = new AlertDialog.Builder(MainActivity.this);
                ab.setTitle("tufani");
                ab.setMessage("Will you Really Want to transfer Data");
                ab.setIcon(R.mipmap.ic_launcher_round);
                ab.setCancelable(false);

                ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       // Toast.makeText(MainActivity.this, "ok Tufani ho gya", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(MainActivity.this,Main2Activity.class)
                                .putExtra("Name",((EditText)findViewById(R.id.edittext1)).getText().toString())
                        );


                    }
                });

                ab.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });


                ab.show();
            }
        });


    }
}
